<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/28/14
 * Time: 12:34 AM
 * Project: objectframework.local
 * File: Collector.php
 */

namespace maintenance\environment;


class Collector
{
    /**
     * collecting input output data, assay, then put into a bowl
     * bowl rounds data across , depends on request i/o or event, it's ok
     * bowl make calculations
     * bowl makes sorting
     * bowl constructing result data
     * bowl send/get data to/from storage
     * bowl is the brain of this application
     * other modules are body for breathe, eat, and sleep.
     * collector collecting : API,GET,POST,URI,EVENT request data.
     */
} 