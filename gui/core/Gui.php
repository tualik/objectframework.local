<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/28/14
 * Time: 12:43 AM
 * Project: objectframework.local
 * File: Gui.php
 */

namespace gui\core;


class Gui
{

    /**
     * @var array
     * $template array,
     * $template[file] - only filename
     * $template[path] - only path
     * $template[extension]  - only file extension
     * $template[skin] - only skin
     */
    private $template = array();

    /**
     * @var array
     * $data array,
     * $data[dom]
     * $data[head]
     * $data[body]
     * $data[header]
     * $data[content]
     * $data[footer]
     * categorized array for light template engine
     */
    private $data = array();

    function __construct($template, $data)
    {
        $this->template = $template;
        $this->data = $data;
    }

    public static function start($template, $data)
    {
        $Gui = new Gui($template, $data);
        $Gui->aggregate_data($template['skin']);
        $Gui->build_GUI($Gui->$template, $Gui->data);
        return true;
    }

    public function build_GUI($template, $data)
    {
        $this->show($template['dom'], $data['dom']);
    }

    public function show($template, $data)
    {
        if (!empty($data)) {
            return $data;
        }
        include("$template[path].$template[file].$template[extension]");
        return true;
    }

    public function aggregate_data($skin)
    {

        $skin = "DefaultSettings";
        if (empty($this->template['dom']) or !isset($this->template['dom'])) {
            $this->template['dom'] = $skin::$dom_template;
        }
        if (empty($template['head']) or !isset($this->template['dom'])) {
            $this->template['head'] = $skin::$head_template;
        }
        if (empty($this->template['body']) or !isset($this->template['body'])) {
            $this->template['body'] = $skin::$body_template;
        }
        if (empty($this->template['header']) or !isset($this->template['header'])) {
            $this->template['header'] = $skin::$header_template;
        }
        if (empty($this->template['content']) or !isset($this->template['content'])) {
            $this->template['content'] = $skin::$content_template;
        }
        if (empty($this->template['footer']) or !isset($this->template['footer'])) {
            $this->template['footer'] = $skin::$footer_template;
        }
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function getTemplate()
    {
        return $this->template;
    }


}