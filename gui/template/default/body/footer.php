<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/28/14
 * Time: 12:42 AM
 * Project: objectframework.local
 * File: footer.php
 */
?>
<footer>
    <?php $this->show($this->template['footer'], $this->data['footer']); ?>
</footer>