<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/28/14
 * Time: 12:41 AM
 * Project: objectframework.local
 * File: body.php
 */

$this->show($this->template['header'], $this->data['header']);
$this->show($this->template['content'], $this->data['content']);
$this->show($this->template['footer'], $this->data['footer']);