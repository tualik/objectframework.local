<?php
/**
 * Created by Tualik.
 * User: aram
 * Date: 5/28/14
 * Time: 12:41 AM
 * Project: objectframework.local
 * File: dom.php
 */
?>
<html>
<head>
    <?php $this->show($this->template['head'], $this->data['head']); ?>
</head>
<body>
<?php $this->show($this->template['body'], $this->data['body']); ?>
</body>
</html>