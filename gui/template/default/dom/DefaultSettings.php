<?php

/**
 * Created by Tualik.
 * User: aram
 * Date: 6/5/14
 * Time: 6:17 PM
 * Project: objectframework.local
 * File: DefaultSettings.php
 */
class DefaultSettings
{
    /**
     * $template construction:
     * $template[file] - only filename
     * $template[path] - only path
     * $template[extension]  - only file extension
     */

    /**
     * @var array
     */
    public static $dom_template = array();

    /**
     * @var array
     */
    public static $head_template = array();

    /**
     * @var array
     */
    public static $body_template = array();

    /**
     * @var array
     */
    public static $header_template = array();

    /**
     * @var array
     */
    public static $content_template = array();

    /**
     * @var array
     */
    public static $footer_template = array();
} 